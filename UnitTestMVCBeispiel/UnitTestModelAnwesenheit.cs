using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCBeispiel;
namespace UnitTestMVCBeispiel
{
    [TestClass]
    public class UnitTestModelAnwesenheit
    {
        [TestMethod]
        public void IModelAnwesenheit_getKlassenNamen()
        {
            //Arrange
            IModelAnwesenheit model = new ModelAnwesenheit();
            
            //Act
            string[] erg = model.getKlassenNamen();

            //Assert
            Assert.IsTrue(erg.Length == 3);
        }
    }
}
