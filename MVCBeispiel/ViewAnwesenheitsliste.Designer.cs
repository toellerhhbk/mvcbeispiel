﻿namespace MVCBeispiel
{
    partial class ViewAnwesenheitsliste
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxKlassennamen = new System.Windows.Forms.ComboBox();
            this.labelKlassennamen = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelVonUstd = new System.Windows.Forms.Label();
            this.labelBisUstd = new System.Windows.Forms.Label();
            this.textBoxDatum = new System.Windows.Forms.TextBox();
            this.numericUpDownVonUstd = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownBisUstd = new System.Windows.Forms.NumericUpDown();
            this.listBoxSchueler = new System.Windows.Forms.ListBox();
            this.checkBoxAnwesend = new System.Windows.Forms.CheckBox();
            this.checkBoxVerspaetung = new System.Windows.Forms.CheckBox();
            this.checkBoxVersaeumnis = new System.Windows.Forms.CheckBox();
            this.groupBoxSchueler = new System.Windows.Forms.GroupBox();
            this.checkBoxEntschuldigtBeurlaubt = new System.Windows.Forms.CheckBox();
            this.checkBoxEntschuldigtKrank = new System.Windows.Forms.CheckBox();
            this.textBoxVersaeumnis = new System.Windows.Forms.TextBox();
            this.textBoxVerspaetung = new System.Windows.Forms.TextBox();
            this.buttonAnwesenheitSpeichern = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVonUstd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBisUstd)).BeginInit();
            this.groupBoxSchueler.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxKlassennamen
            // 
            this.comboBoxKlassennamen.FormattingEnabled = true;
            this.comboBoxKlassennamen.Location = new System.Drawing.Point(12, 32);
            this.comboBoxKlassennamen.Name = "comboBoxKlassennamen";
            this.comboBoxKlassennamen.Size = new System.Drawing.Size(121, 21);
            this.comboBoxKlassennamen.TabIndex = 0;
            this.comboBoxKlassennamen.SelectedIndexChanged += new System.EventHandler(this.comboBoxKlassennamen_SelectedIndexChanged);
            // 
            // labelKlassennamen
            // 
            this.labelKlassennamen.AutoSize = true;
            this.labelKlassennamen.Location = new System.Drawing.Point(13, 13);
            this.labelKlassennamen.Name = "labelKlassennamen";
            this.labelKlassennamen.Size = new System.Drawing.Size(38, 13);
            this.labelKlassennamen.TabIndex = 1;
            this.labelKlassennamen.Text = "Klasse";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(172, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Datum";
            // 
            // labelVonUstd
            // 
            this.labelVonUstd.AutoSize = true;
            this.labelVonUstd.Location = new System.Drawing.Point(282, 12);
            this.labelVonUstd.Name = "labelVonUstd";
            this.labelVonUstd.Size = new System.Drawing.Size(53, 13);
            this.labelVonUstd.TabIndex = 3;
            this.labelVonUstd.Text = "von Ustd.";
            // 
            // labelBisUstd
            // 
            this.labelBisUstd.AutoSize = true;
            this.labelBisUstd.Location = new System.Drawing.Point(373, 13);
            this.labelBisUstd.Name = "labelBisUstd";
            this.labelBisUstd.Size = new System.Drawing.Size(48, 13);
            this.labelBisUstd.TabIndex = 4;
            this.labelBisUstd.Text = "bis Ustd.";
            // 
            // textBoxDatum
            // 
            this.textBoxDatum.Location = new System.Drawing.Point(175, 33);
            this.textBoxDatum.Name = "textBoxDatum";
            this.textBoxDatum.Size = new System.Drawing.Size(97, 20);
            this.textBoxDatum.TabIndex = 5;
            // 
            // numericUpDownVonUstd
            // 
            this.numericUpDownVonUstd.Location = new System.Drawing.Point(285, 33);
            this.numericUpDownVonUstd.Name = "numericUpDownVonUstd";
            this.numericUpDownVonUstd.Size = new System.Drawing.Size(73, 20);
            this.numericUpDownVonUstd.TabIndex = 6;
            // 
            // numericUpDownBisUstd
            // 
            this.numericUpDownBisUstd.Location = new System.Drawing.Point(376, 33);
            this.numericUpDownBisUstd.Name = "numericUpDownBisUstd";
            this.numericUpDownBisUstd.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownBisUstd.TabIndex = 7;
            // 
            // listBoxSchueler
            // 
            this.listBoxSchueler.FormattingEnabled = true;
            this.listBoxSchueler.Location = new System.Drawing.Point(12, 70);
            this.listBoxSchueler.Name = "listBoxSchueler";
            this.listBoxSchueler.Size = new System.Drawing.Size(120, 147);
            this.listBoxSchueler.TabIndex = 8;
            this.listBoxSchueler.SelectedIndexChanged += new System.EventHandler(this.listBoxSchueler_SelectedIndexChanged);
            // 
            // checkBoxAnwesend
            // 
            this.checkBoxAnwesend.AutoSize = true;
            this.checkBoxAnwesend.Location = new System.Drawing.Point(9, 23);
            this.checkBoxAnwesend.Name = "checkBoxAnwesend";
            this.checkBoxAnwesend.Size = new System.Drawing.Size(75, 17);
            this.checkBoxAnwesend.TabIndex = 11;
            this.checkBoxAnwesend.Text = "anwesend";
            this.checkBoxAnwesend.UseVisualStyleBackColor = true;
            // 
            // checkBoxVerspaetung
            // 
            this.checkBoxVerspaetung.AutoSize = true;
            this.checkBoxVerspaetung.Location = new System.Drawing.Point(9, 47);
            this.checkBoxVerspaetung.Name = "checkBoxVerspaetung";
            this.checkBoxVerspaetung.Size = new System.Drawing.Size(80, 17);
            this.checkBoxVerspaetung.TabIndex = 12;
            this.checkBoxVerspaetung.Text = "Verspätung";
            this.checkBoxVerspaetung.UseVisualStyleBackColor = true;
            // 
            // checkBoxVersaeumnis
            // 
            this.checkBoxVersaeumnis.AutoSize = true;
            this.checkBoxVersaeumnis.Location = new System.Drawing.Point(9, 71);
            this.checkBoxVersaeumnis.Name = "checkBoxVersaeumnis";
            this.checkBoxVersaeumnis.Size = new System.Drawing.Size(80, 17);
            this.checkBoxVersaeumnis.TabIndex = 13;
            this.checkBoxVersaeumnis.Text = "Versäumnis";
            this.checkBoxVersaeumnis.UseVisualStyleBackColor = true;
            // 
            // groupBoxSchueler
            // 
            this.groupBoxSchueler.Controls.Add(this.checkBoxEntschuldigtBeurlaubt);
            this.groupBoxSchueler.Controls.Add(this.checkBoxEntschuldigtKrank);
            this.groupBoxSchueler.Controls.Add(this.textBoxVersaeumnis);
            this.groupBoxSchueler.Controls.Add(this.textBoxVerspaetung);
            this.groupBoxSchueler.Controls.Add(this.checkBoxAnwesend);
            this.groupBoxSchueler.Controls.Add(this.checkBoxVersaeumnis);
            this.groupBoxSchueler.Controls.Add(this.checkBoxVerspaetung);
            this.groupBoxSchueler.Location = new System.Drawing.Point(175, 70);
            this.groupBoxSchueler.Name = "groupBoxSchueler";
            this.groupBoxSchueler.Size = new System.Drawing.Size(271, 148);
            this.groupBoxSchueler.TabIndex = 14;
            this.groupBoxSchueler.TabStop = false;
            this.groupBoxSchueler.Text = "Schüler";
            // 
            // checkBoxEntschuldigtBeurlaubt
            // 
            this.checkBoxEntschuldigtBeurlaubt.AutoSize = true;
            this.checkBoxEntschuldigtBeurlaubt.Location = new System.Drawing.Point(9, 119);
            this.checkBoxEntschuldigtBeurlaubt.Name = "checkBoxEntschuldigtBeurlaubt";
            this.checkBoxEntschuldigtBeurlaubt.Size = new System.Drawing.Size(131, 17);
            this.checkBoxEntschuldigtBeurlaubt.TabIndex = 17;
            this.checkBoxEntschuldigtBeurlaubt.Text = "Beurlaubt entschuldigt";
            this.checkBoxEntschuldigtBeurlaubt.UseVisualStyleBackColor = true;
            // 
            // checkBoxEntschuldigtKrank
            // 
            this.checkBoxEntschuldigtKrank.AutoSize = true;
            this.checkBoxEntschuldigtKrank.Location = new System.Drawing.Point(9, 95);
            this.checkBoxEntschuldigtKrank.Name = "checkBoxEntschuldigtKrank";
            this.checkBoxEntschuldigtKrank.Size = new System.Drawing.Size(114, 17);
            this.checkBoxEntschuldigtKrank.TabIndex = 16;
            this.checkBoxEntschuldigtKrank.Text = "Krank entschuldigt";
            this.checkBoxEntschuldigtKrank.UseVisualStyleBackColor = true;
            // 
            // textBoxVersaeumnis
            // 
            this.textBoxVersaeumnis.Location = new System.Drawing.Point(110, 71);
            this.textBoxVersaeumnis.Name = "textBoxVersaeumnis";
            this.textBoxVersaeumnis.Size = new System.Drawing.Size(100, 20);
            this.textBoxVersaeumnis.TabIndex = 15;
            // 
            // textBoxVerspaetung
            // 
            this.textBoxVerspaetung.Location = new System.Drawing.Point(110, 47);
            this.textBoxVerspaetung.Name = "textBoxVerspaetung";
            this.textBoxVerspaetung.Size = new System.Drawing.Size(100, 20);
            this.textBoxVerspaetung.TabIndex = 14;
            // 
            // buttonAnwesenheitSpeichern
            // 
            this.buttonAnwesenheitSpeichern.Location = new System.Drawing.Point(175, 224);
            this.buttonAnwesenheitSpeichern.Name = "buttonAnwesenheitSpeichern";
            this.buttonAnwesenheitSpeichern.Size = new System.Drawing.Size(270, 23);
            this.buttonAnwesenheitSpeichern.TabIndex = 15;
            this.buttonAnwesenheitSpeichern.Text = "Anwesenheit speichern";
            this.buttonAnwesenheitSpeichern.UseVisualStyleBackColor = true;
            this.buttonAnwesenheitSpeichern.Click += new System.EventHandler(this.buttonAnwesenheitSpeichern_Click);
            // 
            // ViewAnwesenheitsliste
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 264);
            this.Controls.Add(this.buttonAnwesenheitSpeichern);
            this.Controls.Add(this.groupBoxSchueler);
            this.Controls.Add(this.listBoxSchueler);
            this.Controls.Add(this.numericUpDownBisUstd);
            this.Controls.Add(this.numericUpDownVonUstd);
            this.Controls.Add(this.textBoxDatum);
            this.Controls.Add(this.labelBisUstd);
            this.Controls.Add(this.labelVonUstd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelKlassennamen);
            this.Controls.Add(this.comboBoxKlassennamen);
            this.Name = "ViewAnwesenheitsliste";
            this.Text = "Anwesenheitsliste";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVonUstd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBisUstd)).EndInit();
            this.groupBoxSchueler.ResumeLayout(false);
            this.groupBoxSchueler.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxKlassennamen;
        private System.Windows.Forms.Label labelKlassennamen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelVonUstd;
        private System.Windows.Forms.Label labelBisUstd;
        private System.Windows.Forms.TextBox textBoxDatum;
        private System.Windows.Forms.NumericUpDown numericUpDownVonUstd;
        private System.Windows.Forms.NumericUpDown numericUpDownBisUstd;
        private System.Windows.Forms.ListBox listBoxSchueler;
        private System.Windows.Forms.CheckBox checkBoxAnwesend;
        private System.Windows.Forms.CheckBox checkBoxVerspaetung;
        private System.Windows.Forms.CheckBox checkBoxVersaeumnis;
        private System.Windows.Forms.GroupBox groupBoxSchueler;
        private System.Windows.Forms.TextBox textBoxVersaeumnis;
        private System.Windows.Forms.TextBox textBoxVerspaetung;
        private System.Windows.Forms.Button buttonAnwesenheitSpeichern;
        private System.Windows.Forms.CheckBox checkBoxEntschuldigtBeurlaubt;
        private System.Windows.Forms.CheckBox checkBoxEntschuldigtKrank;
    }
}

