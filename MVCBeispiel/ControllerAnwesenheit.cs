﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCBeispiel
{
    public class ControllerAnwesenheit : IControllerAnwesenheit
    {
        private IModelAnwesenheit modelAnwesenheit;

        public ControllerAnwesenheit(IModelAnwesenheit ima)
        {
            modelAnwesenheit = ima;
        }
        Anwesenheit IControllerAnwesenheit.getAnwesenheit(string Klassenname, string Nachname, string Vorname, DateTime Datum, int Ustd)
        {
            return modelAnwesenheit.getAnwesenheit(Klassenname, Nachname, Vorname, Datum, Ustd);
        }

        Anwesenheit IControllerAnwesenheit.getAnwesenheit(string Klassenname, string Nachname, string Vorname, DateTime Datum, int vonUstd, int bisUstd)
        {
            return modelAnwesenheit.getAnwesenheit(Klassenname,Nachname,Vorname,Datum,vonUstd, bisUstd);
        }

        
        Schueler[] IControllerAnwesenheit.getKlassenliste(string Klassenname)
        {
            return modelAnwesenheit.getKlassenliste(Klassenname);
        }

        string[] IControllerAnwesenheit.getKlassenNamen()
        {
            return modelAnwesenheit.getKlassenNamen();
            
        }

        void IControllerAnwesenheit.setAnwesenheit(string Klassenname, string Vorname, string Nachname, DateTime Datum, int Ustd, int VerspätungMinuten, int VersäumnisMinuten)
        {
            if (modelAnwesenheit.getAnwesenheit(Klassenname, Nachname, Vorname, Datum, Ustd).Festgestellt)
            {
                modelAnwesenheit.changeAnwesenheit(Klassenname, Vorname, Nachname, Datum, Ustd, VerspätungMinuten, VersäumnisMinuten);

            }
            else
            {
                modelAnwesenheit.setAnwesenheit(Klassenname, Vorname, Nachname, Datum, Ustd, VerspätungMinuten, VersäumnisMinuten);
            }
        }

        void IControllerAnwesenheit.setAnwesenheit(string Klassenname, string Nachname, string Vorname, DateTime Datum, int vonUstd, int bisUstd, int VerspätungMinuten, int VersäumnisMinuten)
        {
            if (modelAnwesenheit.getAnwesenheit(Klassenname, Nachname, Vorname, Datum, vonUstd, bisUstd).Festgestellt)
            {
                for (int ustd = vonUstd; ustd <= bisUstd; ustd++)
                {
                    modelAnwesenheit.changeAnwesenheit(Klassenname, Vorname, Nachname, Datum, ustd, VerspätungMinuten, VersäumnisMinuten);
                }
            }
            else
            {
                for (int ustd = vonUstd; ustd <= bisUstd; ustd++)
                {
                    modelAnwesenheit.setAnwesenheit(Klassenname, Vorname, Nachname, Datum, ustd, VerspätungMinuten, VersäumnisMinuten);
                }
            }

        }

        
    }
}
