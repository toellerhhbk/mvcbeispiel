﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCBeispiel
{
    public interface IModelAnwesenheit
    {
        String[] getKlassenNamen();
        Schueler[] getKlassenliste(String Klassenname);
        void setAnwesenheit(String Klassenname, String Vorname, String Nachname, DateTime Datum, int Ustd, int VerspätungMinuten, int VersäumnisMinuten);
        void changeAnwesenheit(String Klassenname, String Vorname, String Nachname, DateTime Datum, int Ustd, int VerspätungMinuten, int VersäumnisMinuten);
        
        Anwesenheit getAnwesenheit(String Klassenname, String Nachname, String Vorname, DateTime Datum, int Ustd);

        Anwesenheit getAnwesenheit(String Klassenname, String Nachname, String Vorname, DateTime Datum, int vonUstd, int bisUstd);


    }
}
