﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace MVCBeispiel
{
    public class ModelAnwesenheit : IModelAnwesenheit
    {
        private XDocument doc;
        public ModelAnwesenheit()
        {
            try
            {
                //doc = XDocument.Load("anwesenheit.xml");
                doc = XDocument.Load(@"C:\Users\toel\Documents\Source\Repos\2\mvcbeispiel\MVCBeispiel\bin\Debug\anwesenheit.xml");
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                try
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    if (ofd.ShowDialog()==DialogResult.OK)
                    {
                        doc = XDocument.Load(ofd.FileName);
                    }
                }
                catch (Exception exc2)
                {

                    MessageBox.Show(exc2.Message);
                }
                
            }
        }

        void IModelAnwesenheit.changeAnwesenheit(string Klassenname, string Vorname, string Nachname, DateTime Datum, int Ustd, int VerspätungMinuten, int VersäumnisMinuten)
        {
            doc.Descendants("klasse")
                .Where(e => e.Attribute("name").Value == Klassenname)
                .Descendants("schueler")
                .Where(a => a.Attribute("name").Value == Nachname && a.Attribute("vorname").Value == Vorname)
                .Descendants("anwesenheit")
                .First().ReplaceWith(
                new XElement("anwesenheit",
                 new XElement("Datum",
                     new XAttribute("datum", Datum.ToShortDateString()),
                     new XElement("Ustd",
                      new XAttribute("ustd", Ustd),
                     new XElement("Verspätung", VerspätungMinuten),
                     new XElement("Versäumnis", VersäumnisMinuten),
                     new XElement("Entschuldigung", ""),
                     new XElement("Beurlaubung", "")))));
            doc.Save("anwesenheit.xml");
        }

        Anwesenheit IModelAnwesenheit.getAnwesenheit(string Klassenname, string Nachname, string Vorname, DateTime Datum, int Ustd)
        {
            Anwesenheit aw = new Anwesenheit();
            aw.VerspätungMinuten = 0;
            aw.VersäumnisMinuten = 0;
            aw.Festgestellt = false;
            
            IEnumerable<XElement> xe = doc.Descendants("klasse")
                                            .Where(e => e.Attribute("name").Value == Klassenname)
                                            .Descendants("schueler")
                                            .Where(a => a.Attribute("name").Value == Nachname && a.Attribute("vorname").Value == Vorname)
                                            .Descendants("Datum")
                                            .Where(b => b.Attribute("datum").Value == Datum.ToShortDateString())
                                            .Descendants("Ustd")
                                            .Where(c => c.Attribute("ustd").Value == Ustd.ToString());
                       
            foreach (XElement elim in xe)
            {
                    MessageBox.Show(elim.ToString());
            }

            foreach (XElement el in xe)
            {
                    aw.Festgestellt = true;

                    if (el.Descendants("Verspätung").First().Value.ToString() == "" || el.Descendants("Verspätung").First().Value == null)
                        aw.VerspätungMinuten += 0;
                    else
                        aw.VerspätungMinuten += Convert.ToInt32(el.Descendants("Verspätung").First().Value.ToString());


                    if (el.Descendants("Versäumnis").First().Value.ToString() == "" || el.Descendants("Versäumnis").First().Value == null)
                        aw.VersäumnisMinuten += 0;

                    else
                        aw.VersäumnisMinuten += Convert.ToInt32(el.Descendants("Versäumnis").First().Value.ToString());


            }

            
            return aw;
        }

        Anwesenheit IModelAnwesenheit.getAnwesenheit(string Klassenname, string Nachname, string Vorname, DateTime Datum, int vonUstd, int bisUstd)
        {
            Anwesenheit aw = new Anwesenheit();
            aw.VerspätungMinuten = 0;
            aw.VersäumnisMinuten = 0;
            aw.Festgestellt = false;
            for(int i=vonUstd;i<=bisUstd;i++)
            { 
                IEnumerable<XElement> xe = doc.Descendants("klasse")
                                            .Where(e => e.Attribute("name").Value == Klassenname)
                                            .Descendants("schueler")
                                            .Where(a => a.Attribute("name").Value == Nachname && a.Attribute("vorname").Value == Vorname)
                                            .Descendants("Datum")
                                            .Where(b => b.Attribute("datum").Value == Datum.ToShortDateString())
                                            .Descendants("Ustd")
                                            .Where(c => c.Attribute("ustd").Value == i.ToString());

            
            
                foreach (XElement elim in xe)
                {
                    MessageBox.Show(elim.ToString());
                }
                foreach (XElement el in xe)
                {
                    aw.Festgestellt = true;

                    if (el.Descendants("Verspätung").First().Value.ToString() == "" || el.Descendants("Verspätung").First().Value == null)
                        aw.VerspätungMinuten += 0;
                    else
                        aw.VerspätungMinuten += Convert.ToInt32(el.Descendants("Verspätung").First().Value.ToString());


                    if (el.Descendants("Versäumnis").First().Value.ToString() == "" || el.Descendants("Versäumnis").First().Value == null)
                        aw.VersäumnisMinuten += 0;

                    else
                        aw.VersäumnisMinuten += Convert.ToInt32(el.Descendants("Versäumnis").First().Value.ToString());


                }

            }
            return aw;
        }

        Schueler[] IModelAnwesenheit.getKlassenliste(string Klassenname)
        {
            Schueler[] res;
            res = new Schueler[doc.Descendants("klasse").Where(e => e.Attribute("name").Value == Klassenname).Descendants("schueler").Count()];
            int i= 0;
            foreach (XElement elem in doc.Descendants("klasse").Where( e => e.Attribute("name").Value == Klassenname).Descendants("schueler"))
            {
                res[i] = new Schueler(); //Wieso???
               
                res[i].Vorname =elem.Attribute("vorname").Value.ToString();
                res[i].Nachname = elem.Attribute("name").Value.ToString();

               i++;
            }

            return res;
        }

        string[] IModelAnwesenheit.getKlassenNamen()
        {
            string[] res;
            res= new string[doc.Descendants("klasse").Count()];
            
            int i = 0;
            foreach(XElement x in doc.Descendants("klasse"))
            {
                res[i]= x.Attribute("name").Value;
                i++;
            }
            return res;

        }

        void IModelAnwesenheit.setAnwesenheit(string Klassenname, string Vorname, string Nachname, DateTime Datum, int Ustd, int VerspätungMinuten, int VersäumnisMinuten)
        {
            
           doc.Descendants("klasse")
               .Where(e => e.Attribute("name").Value == Klassenname)
               .Descendants("schueler") 
               .Where(a => a.Attribute("name").Value == Nachname && a.Attribute("vorname").Value == Vorname)
               .Descendants("anwesenheit")
               .First().Add(
                new XElement("Datum",
                    new XAttribute("datum", Datum.ToShortDateString()),
                    new XElement("Ustd",
                     new XAttribute("ustd", Ustd),
                    new XElement("Verspätung", VerspätungMinuten),
                    new XElement("Versäumnis", VersäumnisMinuten),
                    new XElement("Entschuldigung", ""),
                    new XElement("Beurlaubung", ""))));
            doc.Save("anwesenheit.xml");
            
                                          

        }
    }
}
