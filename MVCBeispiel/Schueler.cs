﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCBeispiel
{
    public class Schueler
    {
        private String vorname;
        private String nachname;

        public string Vorname { get => vorname; set => vorname = value; }
        public string Nachname { get => nachname; set => nachname = value; }

        override public String ToString()
        {
            return nachname + "," + vorname;
        }
    }
}
