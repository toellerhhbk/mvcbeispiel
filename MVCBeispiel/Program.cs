﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVCBeispiel
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IModelAnwesenheit model = new ModelAnwesenheit();
            IControllerAnwesenheit controllerAnwesenheit = new ControllerAnwesenheit(model);
            IViewAnwesenheitsliste viewAnwesenheitsliste = new ViewAnwesenheitsliste();

            viewAnwesenheitsliste.setController(controllerAnwesenheit);

            
            Application.Run((ViewAnwesenheitsliste)viewAnwesenheitsliste);
        }
    }
}
