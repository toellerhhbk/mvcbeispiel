﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCBeispiel
{
    public class Anwesenheit
    {
        private int verspätungMinuten;
        private int versäumnisMinuten;
        private bool festgestellt;

        public int VerspätungMinuten { get => verspätungMinuten; set => verspätungMinuten = value; }
        public int VersäumnisMinuten { get => versäumnisMinuten; set => versäumnisMinuten = value; }
        public bool Festgestellt { get => festgestellt; set => festgestellt = value; }
    }
}
