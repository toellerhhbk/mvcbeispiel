﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MVCBeispiel
{
    public partial class ViewAnwesenheitsliste : Form, IViewAnwesenheitsliste
    {
        private IControllerAnwesenheit controllerAnwesenheit;

        public ViewAnwesenheitsliste()
        {
            InitializeComponent();

            comboBoxKlassennamen.Items.Clear();
            //Geht erst, wenn der Controller gesetzt wurde!!
            /*
            try
            {
                comboBoxKlassennamen.Items.AddRange(controllerAnwesenheit.getKlassenNamen());
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            */
            textBoxDatum.Text = DateTime.Now.ToShortDateString();
            textBoxVersaeumnis.Text = "0";
            textBoxVerspaetung.Text = "0";

            if (DateTime.Now > Convert.ToDateTime("7:45"))
            {
                numericUpDownVonUstd.Value = 1;
                numericUpDownBisUstd.Value = 1;
            }

        }
        public ViewAnwesenheitsliste(IControllerAnwesenheit ica)
        {
            this.controllerAnwesenheit=ica;
            InitializeComponent();
            comboBoxKlassennamen.Items.Clear();
            try
            {
                comboBoxKlassennamen.Items.AddRange(controllerAnwesenheit.getKlassenNamen());
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            textBoxDatum.Text = DateTime.Now.ToShortDateString();
            textBoxVersaeumnis.Text = "0";
            textBoxVerspaetung.Text = "0";

            if (DateTime.Now>Convert.ToDateTime("7:45"))
            {
                numericUpDownVonUstd.Value = 1;
                numericUpDownBisUstd.Value = 1;
            }

        }
        void IViewAnwesenheitsliste.setController(IControllerAnwesenheit caw)
        {
            controllerAnwesenheit = caw;
            comboBoxKlassennamen.Items.Clear();
            
            try
            {
                comboBoxKlassennamen.Items.AddRange(controllerAnwesenheit.getKlassenNamen());
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void comboBoxKlassennamen_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                listBoxSchueler.Items.Clear();
                foreach (Schueler sue in controllerAnwesenheit.getKlassenliste(comboBoxKlassennamen.SelectedItem.ToString()))
                {
                    listBoxSchueler.Items.Add(sue.ToString());
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        

        private void buttonAnwesenheitSpeichern_Click(object sender, EventArgs e)
        {
            try
            {
                for (int ustd=Convert.ToInt32(numericUpDownVonUstd.Value);ustd<=Convert.ToInt32(numericUpDownBisUstd.Value);ustd++)
            {
                
                    controllerAnwesenheit.setAnwesenheit(
                        comboBoxKlassennamen.SelectedItem.ToString(),
                        groupBoxSchueler.Text.Split(',')[1],
                        groupBoxSchueler.Text.Split(',')[0],
                        Convert.ToDateTime(textBoxDatum.Text),
                        ustd,
                        Convert.ToInt32(textBoxVerspaetung.Text),
                        Convert.ToInt32(textBoxVersaeumnis.Text));
               
            }
                if(listBoxSchueler.SelectedIndex<listBoxSchueler.Items.Count-1)
                    listBoxSchueler.SelectedIndex++;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

        private void listBoxSchueler_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                groupBoxSchueler.Text = listBoxSchueler.SelectedItem.ToString();
                Anwesenheit aw = controllerAnwesenheit.getAnwesenheit(
                    comboBoxKlassennamen.SelectedItem.ToString(),
                    listBoxSchueler.SelectedItem.ToString().Split(',')[0],
                    listBoxSchueler.SelectedItem.ToString().Split(',')[1],
                    Convert.ToDateTime(textBoxDatum.Text),
                    Convert.ToInt32(numericUpDownVonUstd.Value),
                    Convert.ToInt32(numericUpDownBisUstd.Value)
                    );

                checkBoxAnwesend.Checked = false;
                checkBoxVersaeumnis.Checked = false;
                checkBoxVerspaetung.Checked = false;

                if (aw.Festgestellt)
                {
                    checkBoxAnwesend.Checked = true;
                    if (aw.VersäumnisMinuten > 0)
                    {
                        textBoxVersaeumnis.Text = Convert.ToString(aw.VersäumnisMinuten);
                        checkBoxVersaeumnis.Checked = true;
                    }
                    else
                    {
                        
                        textBoxVersaeumnis.Text = "0";
                    }


                    if (aw.VerspätungMinuten > 0)
                    {
                        textBoxVerspaetung.Text = Convert.ToString(aw.VerspätungMinuten);
                        checkBoxVerspaetung.Checked = true;
                    }
                    else
                    {
                        
                        textBoxVerspaetung.Text = "0";
                    }
                }
            }

            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

       
    }
}
